//
//  Extension.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit

extension String {
    /**
     Below method is used to convert string value to double
     */
    func toDouble() -> Double {
        return NumberFormatter().number(from: self)?.doubleValue ?? 0.0
    }
}
