//
//  TrainingHoursTableViewCell.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var detailView: UIView!
    
    @IBOutlet weak var studentCountView: UIView!
    @IBOutlet weak var studentCountLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    func initialize(){
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowRadius = 5
        shadowView.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        shadowView.layer.shadowOpacity = 1.0
        detailView.layer.cornerRadius = 10.0
        studentCountView.layer.cornerRadius = 5
    }
    
    func setCellDetails(withSchoolObj schoolObj: School) {
        schoolName.text = schoolObj.schoolName
        studentCountLabel.text = schoolObj.totalStudents
        adressLabel.text = schoolObj.primaryAddressLine1
        emailLabel.text = schoolObj.schoolEmail
        phoneLabel.text = schoolObj.phoneNumber
        codeLabel.text = schoolObj.code1
        websiteLabel.text = schoolObj.website
        
        if let websiteTxt = schoolObj.website, let url = URL(string: schoolObj.website!) {
            let attributedString = NSMutableAttributedString(string: websiteTxt, attributes:[NSAttributedString.Key.link: url])
            websiteLabel.attributedText = attributedString
            websiteLabel.isUserInteractionEnabled = true
            websiteLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openLink)))
        }
    }
    
    @objc func openLink() {
        if let urlString = self.websiteLabel.text, let url = URL(string: "https://" + urlString) {
            UIApplication.shared.open(url)
        }
    }
}
