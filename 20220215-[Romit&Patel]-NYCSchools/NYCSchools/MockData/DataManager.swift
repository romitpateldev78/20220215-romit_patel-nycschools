//
//  DataManager.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit


class DataManager {
    static let shared = DataManager()
    private init() {}
    var schools: [School] {
        if let path = Bundle.main.path(forResource: "schools", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try JSONDecoder().decode([School].self, from: data)
            } catch {
                
            }
        }
        return []
    }
    var satScore: [SatScore] {
        if let path = Bundle.main.path(forResource: "satscore", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return try JSONDecoder().decode([SatScore].self, from: data)
            } catch {
                
            }
        }
        return []
    }
}
