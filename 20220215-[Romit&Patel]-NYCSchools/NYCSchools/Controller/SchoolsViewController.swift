//
//  ViewController.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit
import MBProgressHUD
class SchoolsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView?
    var schools: [School] =  DataManager.shared.schools

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getSchools()
    }
    
    func getSchools() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.getSchoolsList { [weak self] (response, status) in
            if status {
                if let data = response as? [School] {
                    self?.schools = data
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: (self?.view)!, animated: true)
                        self?.tableView?.reloadData()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: (self?.view)!, animated: true)
                    let alertController = UIAlertController(title: "", message: "Something went wrong!", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    }))
                    self?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}

extension SchoolsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell") as! SchoolTableViewCell
        cell.setCellDetails(withSchoolObj: schools[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = self.storyboard?.instantiateViewController(identifier: "DetailsViewController") as! DetailsViewController
        detailsVC.selectedSchoolId = schools[indexPath.row].dbn ?? ""
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}
