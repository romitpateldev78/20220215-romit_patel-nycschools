//
//  DetailsViewController.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit
import Charts
import MBProgressHUD
class DetailsViewController: UIViewController {
    
    @IBOutlet weak var pieChart: PieChartView?
    @IBOutlet weak var schoolNameLabel: UILabel?
    @IBOutlet weak var codeLabel: UILabel?
    @IBOutlet weak var numOfTestTakersLabel: UILabel?

    var selectedSchoolId: String = ""
    private var schoolDetails:SatScore?
    var satScores: [SatScore] = DataManager.shared.satScore //[]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Details"
        getScore()
    }
    func getScore() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.getScores { [weak self] (response, status) in
            if status {
                if let data = response as? [SatScore] {
                    self?.satScores = data
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: (self?.view)!, animated: true)
                        self?.updateUI()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: (self?.view)!, animated: true)
                    let alertController = UIAlertController(title: "", message: "Something went wrong!", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    }))
                    self?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func updateUI() {
        schoolDetails = self.satScores.first(where: {$0.dbn == selectedSchoolId})
        setSchoolDetails()
        setPieChartProperties()
    }
    
    private func setSchoolDetails() {
        schoolNameLabel?.text = schoolDetails?.schoolName
        codeLabel?.text = schoolDetails?.dbn
        numOfTestTakersLabel?.text = schoolDetails?.numOfSatTestTakers
    }
    
    /**
     configures piechart and sets the datasource with average score details.
     */
    private func setPieChartProperties(){
        pieChart?.holeRadiusPercent = CGFloat(0.5)
        pieChart?.transparentCircleColor = .clear
        pieChart?.holeColor = UIColor.white
        pieChart?.drawSlicesUnderHoleEnabled = true
        pieChart?.drawEntryLabelsEnabled = false

        let legend = pieChart?.legend
        legend?.horizontalAlignment = .right
        legend?.verticalAlignment = .top
        legend?.orientation = .vertical
        legend?.xEntrySpace = 7
        
        DispatchQueue.main.async {
            self.pieChart?.data = PieChartData(dataSet: self.getDataSet())
            self.pieChart?.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .easeOutSine)
        }
    }
    /**
        Fetchings PieChartDataSet
     */
    private func getDataSet() ->  PieChartDataSet {
        if  let readScr =  schoolDetails?.satCriticalReadingAvgScore,
            let mathScr = schoolDetails?.satMathAvgScore,
            let writeScr = schoolDetails?.satWritingAvgScore {
            
            let dataSet = PieChartDataSet(entries:
                [
                    PieChartDataEntry(value: readScr.toDouble(), label: "Reading Average score"),
                    PieChartDataEntry(value: mathScr.toDouble(),label: "Math Average score"),
                    PieChartDataEntry(value: writeScr.toDouble(),label: "Writing Average score")
            ],label: nil)
             dataSet.colors = [.systemIndigo, .systemTeal, .gray]
            return dataSet
        }
        return PieChartDataSet()
    }
}

