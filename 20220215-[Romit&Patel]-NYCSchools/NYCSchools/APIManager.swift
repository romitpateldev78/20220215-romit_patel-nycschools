//
//  APIManager.swift
//  NYCSchools
//
//  Created by Romit Patel on 15/02/22.
//

import UIKit
import Foundation

enum EndPoint: String {
    case schools = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case score = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

typealias Completion = (_ response: Any, _ status: Bool) -> Void

enum HttpCodeType: String {
    case get = "GET"
    case post = "POST"
}

struct APIManager {
    private init() {}
    static let shared = APIManager()
    
    func httpRequest(httpCodeType: HttpCodeType, parameters: [String: Any], urlString: String, handler: @escaping Completion) {
        let headers = ["content-type": "application/json", "accept": "application/json"]
        var request = URLRequest(url: URL(string: urlString)!)
        request.allHTTPHeaderFields = headers
        if httpCodeType == HttpCodeType.post {
            var postData:Data?
            do {
                try  postData = JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            } catch { }
            request.httpMethod = HttpCodeType.post.rawValue
            request.httpBody = postData
        } else {
            request.httpMethod = HttpCodeType.get.rawValue
        }
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            switch urlString {
            case EndPoint.schools.rawValue:
                do {
                    let data = try JSONEncoder().encode(DataManager.shared.schools)
                    handler(data, true)
                } catch { }
                break
            case EndPoint.score.rawValue:
                do {
                    let data = try JSONEncoder().encode(DataManager.shared.satScore)
                    handler(data, true)
                } catch { }
                break
            default:
                handler("", false)
            }
            return;
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                print(error ?? "error")
                handler(String(), false)
                
            } else {
                handler(data ?? "", true)
            }
        }.resume()
    }
    
    func getSchoolsList(_ completion: @escaping Completion) {
        httpRequest(httpCodeType: .get, parameters: [:], urlString: EndPoint.schools.rawValue) { (data, status) in
            if status {
                do {
                    let response = try JSONDecoder().decode([School].self, from: data as! Data)
                    completion(response, true)
                } catch let error {
                    print(error.localizedDescription)
                    completion(error, false)
                }
            } else {
                completion(data, false)
            }
        }
    }
    
    func getScores(_ completion: @escaping Completion) {
        httpRequest(httpCodeType: .get, parameters: [:], urlString: EndPoint.score.rawValue) { (data, status) in
            if status {
                do {
                    let response = try JSONDecoder().decode([SatScore].self, from: data as! Data)
                    completion(response, true)
                } catch let error {
                    completion(error, false)
                }
            } else {
                completion(data, false)
            }
        }
    }
    
}
