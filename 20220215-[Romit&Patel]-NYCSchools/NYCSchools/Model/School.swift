
import Foundation
struct School: Codable {
	let dbn: String?
	let schoolName: String?
	let boro: String?
	let overviewParagraph: String?
	let school10thSeats: String?
	let academicOpportunities1: String?
	let academicOpportunities2: String?
	let ellPrograms: String?
	let neighborhood: String?
	let buildingCode: String?
	let location: String?
	let phoneNumber: String?
	let faxNumber: String?
	let schoolEmail: String?
	let website: String?
	let subway: String?
	let bus: String?
	let grades2018: String?
	let finalgrades: String?
	let totalStudents: String?
	let extracurricularActivities: String?
	let schoolSports: String?
	let attendanceRate: String?
	let pctStuEnoughVariety: String?
	let pctStuSafe: String?
	let schoolAccessibilityDescription: String?
	let directions1: String?
	let requirement11: String?
	let requirement21: String?
	let requirement31: String?
	let requirement41: String?
	let requirement51: String?
	let offerRate1: String?
	let program1: String?
	let code1: String?
	let interest1: String?
	let method1: String?
	let seats9ge1: String?
	let grade9gefilledflag1: String?
	let grade9geapplicants1: String?
	let seats9swd1: String?
	let grade9swdfilledflag1: String?
	let grade9swdapplicants1: String?
	let seats101: String?
	let admissionspriority11: String?
	let admissionspriority21: String?
	let admissionspriority31: String?
	let grade9geapplicantsperseat1: String?
	let grade9swdapplicantsperseat1: String?
	let primaryAddressLine1: String?
	let city: String?
	let zip: String?
	let stateCode: String?
	let latitude: String?
	let longitude: String?
	let communityBoard: String?
	let councilDistrict: String?
	let censusTract: String?
	let bin: String?
	let bbl: String?
	let nta: String?
	let borough: String?

	enum CodingKeys: String, CodingKey {

		case dbn = "dbn"
		case schoolName = "school_name"
		case boro = "boro"
		case overviewParagraph = "overview_paragraph"
		case school10thSeats = "school_10th_seats"
		case academicOpportunities1 = "academicopportunities1"
		case academicOpportunities2 = "academicopportunities2"
		case ellPrograms = "ell_programs"
		case neighborhood = "neighborhood"
		case buildingCode = "building_code"
		case location = "location"
		case phoneNumber = "phone_number"
		case faxNumber = "fax_number"
		case schoolEmail = "school_email"
		case website = "website"
		case subway = "subway"
		case bus = "bus"
		case grades2018 = "grades2018"
		case finalgrades = "finalgrades"
		case totalStudents = "total_students"
		case extracurricularActivities = "extracurricular_activities"
		case schoolSports = "school_sports"
		case attendanceRate = "attendance_rate"
		case pctStuEnoughVariety = "pct_stu_enough_variety"
		case pctStuSafe = "pct_stu_safe"
		case schoolAccessibilityDescription = "school_accessibility_description"
		case directions1 = "directions1"
		case requirement11 = "requirement1_1"
		case requirement21 = "requirement2_1"
		case requirement31 = "requirement3_1"
		case requirement41 = "requirement4_1"
		case requirement51 = "requirement5_1"
		case offerRate1 = "offer_rate1"
		case program1 = "program1"
		case code1 = "code1"
		case interest1 = "interest1"
		case method1 = "method1"
		case seats9ge1 = "seats9ge1"
		case grade9gefilledflag1 = "grade9gefilledflag1"
		case grade9geapplicants1 = "grade9geapplicants1"
		case seats9swd1 = "seats9swd1"
		case grade9swdfilledflag1 = "grade9swdfilledflag1"
		case grade9swdapplicants1 = "grade9swdapplicants1"
		case seats101 = "seats101"
		case admissionspriority11 = "admissionspriority11"
		case admissionspriority21 = "admissionspriority21"
		case admissionspriority31 = "admissionspriority31"
		case grade9geapplicantsperseat1 = "grade9geapplicantsperseat1"
		case grade9swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
		case primaryAddressLine1 = "primary_address_line_1"
		case city = "city"
		case zip = "zip"
		case stateCode = "state_code"
		case latitude = "latitude"
		case longitude = "longitude"
		case communityBoard = "community_board"
		case councilDistrict = "council_district"
		case censusTract = "census_tract"
		case bin = "bin"
		case bbl = "bbl"
		case nta = "nta"
		case borough = "borough"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		dbn = try values.decodeIfPresent(String.self, forKey: .dbn)
		schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName)
		boro = try values.decodeIfPresent(String.self, forKey: .boro)
		overviewParagraph = try values.decodeIfPresent(String.self, forKey: .overviewParagraph)
		school10thSeats = try values.decodeIfPresent(String.self, forKey: .school10thSeats)
		academicOpportunities1 = try values.decodeIfPresent(String.self, forKey: .academicOpportunities1)
		academicOpportunities2 = try values.decodeIfPresent(String.self, forKey: .academicOpportunities2)
		ellPrograms = try values.decodeIfPresent(String.self, forKey: .ellPrograms)
		neighborhood = try values.decodeIfPresent(String.self, forKey: .neighborhood)
		buildingCode = try values.decodeIfPresent(String.self, forKey: .buildingCode)
		location = try values.decodeIfPresent(String.self, forKey: .location)
		phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
		faxNumber = try values.decodeIfPresent(String.self, forKey: .faxNumber)
		schoolEmail = try values.decodeIfPresent(String.self, forKey: .schoolEmail)
		website = try values.decodeIfPresent(String.self, forKey: .website)
		subway = try values.decodeIfPresent(String.self, forKey: .subway)
		bus = try values.decodeIfPresent(String.self, forKey: .bus)
		grades2018 = try values.decodeIfPresent(String.self, forKey: .grades2018)
		finalgrades = try values.decodeIfPresent(String.self, forKey: .finalgrades)
		totalStudents = try values.decodeIfPresent(String.self, forKey: .totalStudents)
		extracurricularActivities = try values.decodeIfPresent(String.self, forKey: .extracurricularActivities)
		schoolSports = try values.decodeIfPresent(String.self, forKey: .schoolSports)
		attendanceRate = try values.decodeIfPresent(String.self, forKey: .attendanceRate)
		pctStuEnoughVariety = try values.decodeIfPresent(String.self, forKey: .pctStuEnoughVariety)
		pctStuSafe = try values.decodeIfPresent(String.self, forKey: .pctStuSafe)
		schoolAccessibilityDescription = try values.decodeIfPresent(String.self, forKey: .schoolAccessibilityDescription)
		directions1 = try values.decodeIfPresent(String.self, forKey: .directions1)
		requirement11 = try values.decodeIfPresent(String.self, forKey: .requirement11)
		requirement21 = try values.decodeIfPresent(String.self, forKey: .requirement21)
		requirement31 = try values.decodeIfPresent(String.self, forKey: .requirement31)
		requirement41 = try values.decodeIfPresent(String.self, forKey: .requirement41)
		requirement51 = try values.decodeIfPresent(String.self, forKey: .requirement51)
		offerRate1 = try values.decodeIfPresent(String.self, forKey: .offerRate1)
		program1 = try values.decodeIfPresent(String.self, forKey: .program1)
		code1 = try values.decodeIfPresent(String.self, forKey: .code1)
		interest1 = try values.decodeIfPresent(String.self, forKey: .interest1)
		method1 = try values.decodeIfPresent(String.self, forKey: .method1)
		seats9ge1 = try values.decodeIfPresent(String.self, forKey: .seats9ge1)
		grade9gefilledflag1 = try values.decodeIfPresent(String.self, forKey: .grade9gefilledflag1)
		grade9geapplicants1 = try values.decodeIfPresent(String.self, forKey: .grade9geapplicants1)
		seats9swd1 = try values.decodeIfPresent(String.self, forKey: .seats9swd1)
		grade9swdfilledflag1 = try values.decodeIfPresent(String.self, forKey: .grade9swdfilledflag1)
		grade9swdapplicants1 = try values.decodeIfPresent(String.self, forKey: .grade9swdapplicants1)
		seats101 = try values.decodeIfPresent(String.self, forKey: .seats101)
		admissionspriority11 = try values.decodeIfPresent(String.self, forKey: .admissionspriority11)
		admissionspriority21 = try values.decodeIfPresent(String.self, forKey: .admissionspriority21)
		admissionspriority31 = try values.decodeIfPresent(String.self, forKey: .admissionspriority31)
		grade9geapplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9geapplicantsperseat1)
		grade9swdapplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9swdapplicantsperseat1)
		primaryAddressLine1 = try values.decodeIfPresent(String.self, forKey: .primaryAddressLine1)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		zip = try values.decodeIfPresent(String.self, forKey: .zip)
		stateCode = try values.decodeIfPresent(String.self, forKey: .stateCode)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		communityBoard = try values.decodeIfPresent(String.self, forKey: .communityBoard)
		councilDistrict = try values.decodeIfPresent(String.self, forKey: .councilDistrict)
		censusTract = try values.decodeIfPresent(String.self, forKey: .censusTract)
		bin = try values.decodeIfPresent(String.self, forKey: .bin)
		bbl = try values.decodeIfPresent(String.self, forKey: .bbl)
		nta = try values.decodeIfPresent(String.self, forKey: .nta)
		borough = try values.decodeIfPresent(String.self, forKey: .borough)
	}

}
