//
//  SchoolsViewControllerTests.swift
//  NYCSchoolsTests
//
//  Created by Romit Patel on 15/02/22.
//
import XCTest
@testable import NYCSchools
class SchoolsViewControllerTests: XCTestCase {
    var view: SchoolsViewController! = nil
    override func setUpWithError() throws {
        view = SchoolsViewController()
        XCTAssertNotNil(view)
    }

    func testGetSchools() {
        XCTAssertNotNil(view.getSchools())
        XCTAssertTrue(view.schools.count > 0)
    }
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
