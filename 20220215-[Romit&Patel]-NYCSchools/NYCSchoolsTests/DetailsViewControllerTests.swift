//
//  DetailsViewControllerTests.swift
//  NYCSchoolsTests
//
//  Created by Romit Patel on 15/02/22.
//

import XCTest
@testable import NYCSchools
class DetailsViewControllerTests: XCTestCase {
    var view: DetailsViewController! = nil
    override func setUpWithError() throws {
        view = DetailsViewController()
        XCTAssertNotNil(view)
    }
    func testGetScore() {
        XCTAssertNotNil(view.getScore())
        XCTAssert(view.satScores.count > 0)
    }
    override func tearDownWithError() throws {

    }
}
